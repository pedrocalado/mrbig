# [0.3.0](https://github.com/mrbig-cloud/mrbig/compare/0.2.1...0.3.0) (2020-10-13)

### Features

* Generate Cargo.toml files from templates to manage dependencies' versions.
* Bump `tonic` version to 0.3.1, changelog [here](https://github.com/hyperium/tonic/blob/v0.3.1/CHANGELOG.md).


# [0.2.1](https://github.com/mrbig-cloud/mrbig/compare/0.2.0...0.2.1) (2020-05-25)

### Features

* Allow loading config with vec of args.
* Allow initializing service with vec of args (documented [here](mrbig_core/README.md#from-vec-of-strings)).


# [0.2.0](https://github.com/mrbig-cloud/mrbig/compare/0.1.3...0.2.0) (2020-05-14)

### Chores

* Deprecate `health_check` crate in favor or [tonic-health](https://github.com/hyperium/tonic/tree/v0.2.1/tonic-health).
* Bump `tonic` version to 0.2.1, changelog [here](https://github.com/hyperium/tonic/blob/v0.2.1/CHANGELOG.md).

### Breaking Changes

* Method init() from derived `Run` trait is now async.


# [0.1.3](https://github.com/mrbig-cloud/mrbig/compare/0.1.2...0.1.3) (2020-05-13)

### Bug Fixes

* Fix `0.1.2` known issue  of tracing messages ([23](https://github.com/mrbig-cloud/mrbig/pull/23))) (test with integration tests).
* Delay the health server test to prevent premature failures.


# [0.1.2](https://github.com/mrbig-cloud/mrbig/compare/0.1.0...0.1.2) (2020-05-12)


### Bug Fixes

* Set trace logging filter only if debug is active ([#18](https://github.com/mrbig-cloud/mrbig/pull/18))
* Fix mrbig_derive compilation with tokio feature extra-traits.


### Features

* **mrbig_core**: Add config parameter to set env_logger filters.
* **mrbig_core**: Add traceable feature for tracing messages ([#19](https://github.com/mrbig-cloud/mrbig/pull/19))
* **mrbig_derive**: Add macro for injecting code in service trait impl ([#20](https://github.com/mrbig-cloud/mrbig/pull/20))
* **mrbig_derive**: Add basic prometheus telemetry ([#21](https://github.com/mrbig-cloud/mrbig/pull/21))
* Use github workflow for continuous integration ([#23](https://github.com/mrbig-cloud/mrbig/pull/23))

### Known Issues

* **mrbig_derive**: `service_impl` macro injects tracing message around a simple block `{ }` instead of an async block `async { }`, which does not trap early returns. In short, responses given with early returns are not traced (more [here](https://github.com/mrbig-cloud/mrbig/pull/23)).


# [0.1.0](https://github.com/mrbig-cloud/mrbig/releases/tag/0.1.0) (2020-03-02)


### Initial Version

Our first version with minimal features.
