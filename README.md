# MrBig Just-on-Time Cloud-Native Solutions Crafting

## Overview

`Mr. Big` is a flexible and fast framework for building and deploying Rust cloud-native applications (CNAs) in a constant flow.

`Mr. Big` includes an API to ease the design and implementation of CNAs based on micro-service architecture and Rust.

**This project is no longer being actively maintained, since late 2020.**

## Architecture

The `Mr. Big` ecosystem provides several components.

Check the READMEs for each of them:
* [mrbig_core](mrbig_core/README.md)
* [mrbig_derive](mrbig_derive/README.md)
* [mrbig_build](mrbig_build/README.md)

Find our RFCs [here](https://gitlab.com/pedrocalado/mrbig-rfcs)

## Examples

Checkout our working examples in the [examples](./examples) folder:
* `hotel`: a basic use of mrbig, hello world style.
* `file-storage`: a simple implementation of an object storage using s3 as a backend.
* `event-broker`: an implementation of an in memory message broker.

## Testing

To run all tests, `cd` into the root of the repo and run:

```bash
$ cargo make test
```

To only run the unit tests, do instead:

```bash
$ cargo make unit-tests
```

## Contributing

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, shall be licensed according to the included license, without any additional terms or conditions.
If you want to contribute to `Mr. Big`, please read our [CONTRIBUTING notes](CONTRIBUTING.md).

## Credits and License

Incubated at EVOOQ, original idea by bobbie.ai.

This repository is purposefully unlicensed and falls under the `No License` terms described [here](https://choosealicense.com/no-permission/).

## References

* [Pulumi](https://www.pulumi.com/) - Cloud-native applications deployment solution
* [Cortex](https://www.cortex.dev/) - ML pipeline deployment solution
* [Quarkus](https://quarkus.io/)
* [The Clean Architecture the Right Way - 2019](https://medium.com/gdg-vit/clean-architecture-the-right-way-d83b81ecac6)
* [The Clean Code Blog](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
* [Codicons](https://microsoft.github.io/vscode-codicons/dist/codicon.html)
