use crate::protos::event_broker::{Event, HistoryRequest};
use async_channel::Sender;
use tonic::async_trait;

#[async_trait]
pub trait Producer {
    async fn produce(&self, event: Event) -> Result<(), String>;
}

#[derive(Debug)]
pub enum HistoryConsumerResponse {
    Error(String),
    Event(Event),
    Eof,
}

#[derive(Debug)]
pub struct HistoryConsumerRequest {
    pub request: HistoryRequest,
    pub tx: Sender<HistoryConsumerResponse>,
}

#[async_trait]
pub trait HistoryConsumer {
    async fn consume(&self, request: HistoryConsumerRequest) -> Result<(), String>;
}
