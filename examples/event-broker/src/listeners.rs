use crate::multicast::{Multicast, MulticastConfig};
use crate::protos::event_broker::{ConsumeRequest, Event};
use async_channel::Sender;
#[cfg(not(test))]
use log::debug;
use regex::RegexSet;
use std::collections::HashMap;
#[cfg(test)]
use std::println as debug;
use std::sync::Arc;
use tokio::sync::RwLock;

pub struct RegisterRequest {
    pub consume_request: ConsumeRequest,
    pub tx: Sender<Event>,
}

// Router holds an internal registry of consumer regex patterns.
// For each pattern, it runs an async task (MulticastTask) to send
// incoming events to each consumer of that pattern.
// Each pattern may have multiple associated consumers.
// MulticastTasks hold the sender parts of the consumer channels for the given pattern.
#[derive(Debug)]
struct Router {
    matcher: RegexSet,
    channels: HashMap<String, Multicast>,
}

impl Router {
    fn new() -> Self {
        Self {
            matcher: RegexSet::new(&["^$"]).unwrap(),
            channels: HashMap::new(),
        }
    }

    // This method returns the received request input if requested pattern
    // does not yet exist.
    async fn register_existing_pattern(&self, request: RegisterRequest) -> Option<RegisterRequest> {
        let pattern = &request.consume_request.pattern;
        if let Some(channel) = self.channels.get(pattern) {
            // this clone is only used if adding sender fails
            let tx_clone = request.tx.clone();

            // return the same request if we fail to register
            return match channel.add_sender(request.tx).await {
                Ok(_) => None,
                Err(_) => Some(RegisterRequest {
                    consume_request: request.consume_request,
                    tx: tx_clone,
                }),
            };
        }

        // return the original request since pattern does not exist
        Some(request)
    }

    // register creates a new MulticastTask for the given pattern.
    // If the pattern already exists, register adds a sender to running task.
    async fn register(&mut self, request: RegisterRequest) -> Result<(), String> {
        // garbage collect unused patterns
        self.drop_not_sane();

        let pattern = request.consume_request.pattern;
        let sender = request.tx;
        let mut add_pattern = true;
        if let Some(channel) = self.channels.get_mut(&pattern) {
            add_pattern = false;
            if channel.is_sane() {
                return channel.add_sender(sender).await.map_err(|e| e.to_string());
            }
        }

        // Register pattern to listen.
        let config = MulticastConfig {
            name: pattern.clone(),
            register_timeout: std::time::Duration::new(1, 0),
        };
        self.channels
            .insert(pattern.clone(), Multicast::new(sender, config));

        if !add_pattern {
            return Ok(());
        }

        let new_patterns: Vec<String> = self
            .matcher
            .patterns()
            .iter()
            .map(|s| s.to_string())
            .chain(vec![pattern].into_iter())
            .collect();

        self.set_patterns(&new_patterns).map_err(|e| e.to_string())
    }

    // route hands over an event to all MulticastTasks whose pattern matches the event's topic.
    // (MulticastTask then sends event to registered consumers)
    fn route(&self, event: Event) -> Result<(), String> {
        let topic = &event.topic;
        let matches: Vec<_> = self.matcher.matches(topic).into_iter().collect();
        if matches.len() == 0 {
            return Ok(());
        }

        let results: Result<Vec<_>, _> = matches
            .into_iter()
            .map(|index| {
                let pattern = &self.matcher.patterns()[index];
                // if the channel is not sane, ignore it.
                // it will be garbage collected later.
                match self.channels.get(pattern) {
                    Some(multi) if multi.is_sane() => multi.send(event.clone()),
                    _ => Ok(()),
                }
            })
            .collect();
        results.map(|_| ()).map_err(|e| e.to_string())
    }

    fn drop_not_sane(&mut self) {
        if self.channels.iter().all(|(_, channel)| channel.is_sane()) {
            return;
        }

        let (sane, not_sane): (Vec<(String, bool)>, Vec<(String, bool)>) = self
            .channels
            .iter()
            .map(|(key, channel)| (key.to_string(), channel.is_sane()))
            .partition(|(_, sane)| *sane);

        debug!(
            "dropping patterns: {:?}",
            not_sane
                .iter()
                .fold(String::new(), |s, (key, _)| match s.is_empty() {
                    true => format!("{}", key.as_str()),
                    false => format!("{}, {}", s, key.as_str()),
                })
        );

        // delete map entries
        not_sane.into_iter().for_each(|(key, _)| {
            self.channels.remove(key.as_str());
        });

        let new_patterns: Vec<String> = sane.into_iter().map(|(key, _)| key).collect();

        // we've only removed patterns, so this cannot panic.
        self.set_patterns(&new_patterns).unwrap();
    }

    fn set_patterns<I, S>(&mut self, patterns: I) -> Result<(), regex::Error>
    where
        S: AsRef<str>,
        I: IntoIterator<Item = S>,
    {
        let new_regex = RegexSet::new(patterns)?;
        let _ = std::mem::replace(&mut self.matcher, new_regex);
        Ok(())
    }
}

// ListenerRouter is a thread-safe wrapper around Router.
#[derive(Debug)]
pub struct ListenerRouter {
    inner: Arc<RwLock<Router>>,
}

impl Clone for ListenerRouter {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl ListenerRouter {
    pub fn new() -> Self {
        Self {
            inner: Arc::new(RwLock::new(Router::new())),
        }
    }

    // register handles a request to add a consumer with a matching pattern.
    pub async fn register(&self, request: RegisterRequest) -> Result<(), String> {
        let mut request = request;

        {
            let router = self.inner.read().await;

            let opt = (*router).register_existing_pattern(request).await;
            // pattern exists, so we can exit
            if opt.is_none() {
                return Ok(());
            }

            request = opt.unwrap();
        }

        let mut router = self.inner.write().await;
        (*router).register(request).await
    }

    pub async fn route(&self, event: Event) -> Result<(), String> {
        let router = self.inner.read().await;
        (*router).route(event)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use async_channel::{unbounded, Receiver};

    fn get_now() -> u64 {
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_millis() as u64
    }

    async fn send_receive(reg: &ListenerRouter, rx: &Receiver<Event>, topic: &str) {
        let payload = "some message".as_bytes().to_vec();
        reg.route(Event {
            timestamp: get_now(),
            topic: topic.to_string(),
            key: "bar".into(),
            payload: payload.clone(),
        })
        .await
        .unwrap();

        let event = tokio::time::timeout(std::time::Duration::from_millis(500), rx.recv())
            .await
            .unwrap()
            .unwrap();
        assert_eq!(event.payload, payload);
    }

    async fn send_not_receive(reg: &ListenerRouter, rx: &Receiver<Event>, topic: &str) {
        let payload = "some message".as_bytes().to_vec();
        reg.route(Event {
            timestamp: get_now(),
            topic: topic.to_string(),
            key: "bar".into(),
            payload: payload.clone(),
        })
        .await
        .unwrap();

        tokio::time::timeout(std::time::Duration::from_millis(500), rx.recv())
            .await
            .expect_err("should not have received anything");
    }

    // Simple registry test.
    // Register consumer in "foobar"
    // produce one message in "foobar"
    // expect one message to be consumed
    // produce on another topic
    // expect zero messages to be consumed
    #[test]
    fn listener_registry_register() {
        let (tx, rx) = unbounded();

        let reg = ListenerRouter::new();

        tokio::runtime::Runtime::new()
            .unwrap()
            .block_on(async move {
                // Register a consumer
                reg.register(RegisterRequest {
                    consume_request: ConsumeRequest {
                        pattern: r#"^foobar$"#.into(),
                    },
                    tx,
                })
                .await
                .unwrap();

                // Produce a message and expect to receive one
                {
                    send_receive(&reg, &rx, "foobar").await;
                }

                // Expect to receive zero messages
                {
                    send_not_receive(&reg, &rx, "barfoo").await;
                }
            });
    }

    // Register consumer in "foobar"
    // produce one message in "foobar"
    // expect one message to be consumed
    // Unregister the consumer
    // produce one message in "foobar", expect to succeed
    // Re-register consumer.
    // produce one message in "foobar"
    // expect one message to be consumed
    #[test]
    fn listener_registry_register_twice() {
        let (tx, rx) = unbounded();

        let reg = ListenerRouter::new();

        tokio::runtime::Runtime::new()
            .unwrap()
            .block_on(async move {
                println!("register first consumer");

                // Register a consumer
                reg.register(RegisterRequest {
                    consume_request: ConsumeRequest {
                        pattern: r#"^foobar$"#.into(),
                    },
                    tx,
                })
                .await
                .unwrap();

                println!("send message");

                // Produce a message and expect to receive one
                {
                    send_receive(&reg, &rx, "foobar").await;
                }

                println!("drop consumer");

                // Drop consumer
                drop(rx);

                println!("produce another message");

                // Produce another message
                {
                    reg.route(Event {
                        timestamp: get_now(),
                        topic: "foobar".into(),
                        key: "bar".into(),
                        payload: "fooo".as_bytes().to_vec(),
                    })
                    .await
                    .unwrap(); // must succeed
                }

                println!("register again");

                // Register a new consumer
                let (tx, rx) = unbounded();
                reg.register(RegisterRequest {
                    consume_request: ConsumeRequest {
                        pattern: r#"^foobar$"#.into(),
                    },
                    tx,
                })
                .await
                .unwrap();

                println!("produce yet another");

                // Produce yet another message
                {
                    send_receive(&reg, &rx, "foobar").await;
                }
            });
    }
}
