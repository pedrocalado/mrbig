use crate::protos::event_broker::Event;
use async_channel::{unbounded, Receiver, RecvError, Sender, TrySendError};
#[cfg(not(test))]
use log::{debug, error};
use std::fmt;
#[cfg(test)]
use std::{println as debug, println as error};

enum MulticastMessage {
    Event(Event),
    // A message to register a sender and receive a response
    AddSender((Sender<Event>, Sender<()>)),
}

#[derive(Debug)]
pub enum MulticastError {
    Send(String),
    Recv(RecvError),
    Timeout,
}

impl fmt::Display for MulticastError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MulticastError::Send(e) => write!(f, "multicast send error: {}", e),
            MulticastError::Recv(e) => write!(f, "multicast receive error: {}", e),
            MulticastError::Timeout => write!(f, "multicast timeout"),
        }
    }
}

// MulticastTask listens for events, clones them and sends them
// using all senders in an internal list.
// Senders are appended at runtime.
// For each event consumed, MulticastTask sends as many as number of internal senders.
pub struct MulticastTask {
    name: String,
    rx: Receiver<MulticastMessage>,
}

impl MulticastTask {
    // exec runs the multicast task which listens on a channel for:
    // - requests to add senders to the inner list of senders.
    // - incoming events and uses all senders in the inner list to send the event.
    // If any sender fails to send an event, it is removed from the inner list.
    pub async fn exec(self, tx: Sender<Event>) {
        let MulticastTask { name, rx } = self;
        let mut senders = vec![tx];

        loop {
            match rx.recv().await {
                Ok(message) => match message {
                    MulticastMessage::AddSender((sender, to_respond)) => {
                        debug!("{}: multicast new sender", name);
                        senders.push(sender);
                        to_respond
                            .try_send(())
                            .unwrap_or_else(|e| error!("failed to respond: {}", e));
                    }
                    MulticastMessage::Event(event) => {
                        let mut errors = vec![];
                        // make just enough clones of the event for sending
                        let mut clones: Vec<Event> = [&event]
                            .iter()
                            .map(|c| (*c).clone())
                            .cycle()
                            .take(senders.len() - 1)
                            .collect();
                        clones.push(event);

                        senders.iter().enumerate().zip(clones.into_iter()).for_each(
                            |((i, tx), event)| match tx.try_send(event) {
                                Ok(()) => {}
                                Err(err) => errors.push((i, err)),
                            },
                        );

                        // Drop senders with errors
                        // start from end of array.
                        errors.into_iter().rev().for_each(|(i, err)| {
                            senders.remove(i);
                            match err {
                                TrySendError::Closed(_) => {
                                    debug!("{}: sender removed: {}", name, err);
                                }
                                msg => {
                                    error!("{}: sender removed: {}", name, msg);
                                }
                            }
                        });
                    }
                },
                Err(err) => {
                    // Channel is empty and closed
                    debug!("{}: closing with: {}", name, err);
                    break;
                }
            }

            if senders.len() == 0 {
                debug!("{}: closing with no listeners", name);
                rx.close();
                break;
            }

            if rx.is_closed() {
                debug!("{}: was closed", name);
                break;
            }
        }
    }
}

#[derive(Debug)]
pub struct MulticastConfig {
    pub name: String,
    pub register_timeout: std::time::Duration,
}

#[derive(Debug)]
pub struct Multicast {
    task_tx: Sender<MulticastMessage>,
    register_timeout: std::time::Duration,
}

impl Multicast {
    pub fn new(tx: Sender<Event>, config: MulticastConfig) -> Self {
        let (task_tx, task_rx) = unbounded();

        let MulticastConfig {
            name,
            register_timeout,
        } = config;

        let task = MulticastTask { name, rx: task_rx };

        tokio::spawn(async move { task.exec(tx).await });

        Multicast {
            task_tx,
            register_timeout,
        }
    }

    pub async fn add_sender(&self, tx: Sender<Event>) -> Result<(), MulticastError> {
        let (req_tx, req_rx) = unbounded();
        self.task_tx
            .try_send(MulticastMessage::AddSender((tx, req_tx)))
            .map_err(|e| MulticastError::Send(e.to_string()))?;

        tokio::time::timeout(self.register_timeout, req_rx.recv())
            .await
            .map_err(|_| MulticastError::Timeout)?
            .map_err(|e| MulticastError::Recv(e))
    }

    pub fn send(&self, event: Event) -> Result<(), MulticastError> {
        self.task_tx
            .try_send(MulticastMessage::Event(event))
            .map_err(|e| MulticastError::Send(e.to_string()))
    }

    pub fn is_sane(&self) -> bool {
        !self.task_tx.is_closed()
    }
}
