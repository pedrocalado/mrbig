pub mod event_broker {
    tonic::include_proto!("event_broker");

    pub type EventStream = std::pin::Pin<
        Box<dyn tokio::stream::Stream<Item = Result<Event, tonic::Status>> + Send + Sync + 'static>,
    >;
}
