#[cfg(not(test))]
#[tokio::main]
async fn main() {
    event_broker::exec(std::env::args().collect()).await;
}
