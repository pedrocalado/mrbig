use crate::brokers::{HistoryConsumerRequest, HistoryConsumerResponse};
use crate::listeners::ListenerRouter;
use crate::protos::event_broker::Event;
use async_channel::{unbounded, Receiver, Sender};
#[cfg(not(test))]
use log::{debug, error};
use serde_derive::Deserialize;
use std::collections::{HashMap, VecDeque};
#[cfg(test)]
use std::{println as debug, println as error};
use tonic::async_trait;

#[derive(Debug, Deserialize)]
pub struct VolatileConfig {
    pub queue_size: usize,
    pub topics: Vec<String>,
}

impl Default for VolatileConfig {
    fn default() -> Self {
        Self {
            queue_size: 100,
            topics: vec!["foobar".into()],
        }
    }
}

pub struct VolatileProducer {
    tx: Sender<Event>,
}

#[async_trait]
impl crate::brokers::Producer for VolatileProducer {
    async fn produce(&self, event: Event) -> Result<(), String> {
        self.tx.try_send(event).map_err(|e| e.to_string())
    }
}

#[derive(Debug)]
enum HistoryMessage {
    Event(Event),
    Request(HistoryConsumerRequest),
}

pub struct VolatileHistory {
    tx: Sender<HistoryMessage>,
}

#[async_trait]
impl crate::brokers::HistoryConsumer for VolatileHistory {
    async fn consume(&self, consume: HistoryConsumerRequest) -> Result<(), String> {
        self.tx
            .try_send(HistoryMessage::Request(consume))
            .map_err(|e| e.to_string())
    }
}

struct HistoryThread {
    queues: HashMap<String, VecDeque<Event>>,
}

fn send(tx: &Sender<HistoryConsumerResponse>, response: HistoryConsumerResponse) {
    match tx.try_send(response) {
        Ok(_) => {}
        Err(e) => {
            error!("error sending: {}", e);
        }
    }
}

fn iterate_send<'a, T: Iterator<Item = &'a Event>>(iter: T, tx: &Sender<HistoryConsumerResponse>) {
    iter.for_each(|event: &Event| send(tx, HistoryConsumerResponse::Event(event.clone())));
}

impl HistoryThread {
    async fn start(self, rx: Receiver<HistoryMessage>) {
        let mut this = self;
        loop {
            let result = rx.recv().await;
            if result.is_err() {
                debug!("channel is closed, leaving: {}", result.unwrap_err());
                break;
            }

            match result.unwrap() {
                HistoryMessage::Event(event) => this.append(event),
                HistoryMessage::Request(hcr) => this.request(hcr).await,
            }
        }
    }

    fn append(&mut self, event: Event) {
        let queue = match self.queues.get_mut(&event.topic) {
            Some(q) => q,
            None => {
                return;
            }
        };

        if queue.len() == queue.capacity() {
            queue.pop_front();
        }

        queue.push_back(event);
    }

    async fn request(&self, hcr: HistoryConsumerRequest) {
        let HistoryConsumerRequest { request, tx } = hcr;

        // Check if topic exists
        let opt_queue = self.queues.get(&request.topic);
        if opt_queue.is_none() {
            let msg = format!("no such topic: {}", &request.topic);
            debug!("{}", msg);
            send(&tx, HistoryConsumerResponse::Error(msg));
            return;
        }

        let queue = opt_queue.unwrap();
        let iter = queue.iter();

        // Iterate based on criteria
        match request.criteria {
            Some(criteria) => match (criteria.newest_first, criteria.limit > 0) {
                (false, false) => iterate_send(iter, &tx),
                (false, true) => iterate_send(iter.take(criteria.limit as usize), &tx),
                (true, false) => iterate_send(iter.rev(), &tx),
                (true, true) => iterate_send(iter.rev().take(criteria.limit as usize), &tx),
            },
            None => iterate_send(iter, &tx),
        };

        send(&tx, HistoryConsumerResponse::Eof);
    }
}

struct ConsumerThread {}

impl ConsumerThread {
    async fn start(self, rx: Receiver<Event>, router: ListenerRouter, tx: Sender<HistoryMessage>) {
        loop {
            let result = rx.recv().await;
            if result.is_err() {
                debug!("channel is closed, leaving: {}", result.unwrap_err());
                break;
            }

            let event = result.unwrap();

            match router.route(event.clone()).await {
                Ok(_) => {}
                Err(e) => {
                    error!("failed to route event: {}", e);
                }
            }

            // send to history thread
            let result = tx.try_send(HistoryMessage::Event(event));
            if result.is_err() {
                error!("failed to send to history: {}", result.unwrap_err());
            }
        }
    }
}

pub fn volatile_start(
    router: ListenerRouter,
    cfg: VolatileConfig,
) -> (VolatileProducer, VolatileHistory) {
    let VolatileConfig { queue_size, topics } = cfg;

    let history_queues = topics.into_iter().map(|topic| {
        let queue = VecDeque::with_capacity(queue_size);
        (topic, queue)
    });

    // Start history thread
    let (history_tx, history_rx) = unbounded();
    {
        let history = HistoryThread {
            queues: history_queues.into_iter().collect(),
        };
        tokio::spawn(history.start(history_rx));
    }

    // Start consumer thread
    let (consumer_tx, consumer_rx) = unbounded();
    {
        let consumer = ConsumerThread {};

        tokio::spawn(consumer.start(consumer_rx, router, history_tx.clone()));
    }

    (
        VolatileProducer { tx: consumer_tx },
        VolatileHistory { tx: history_tx },
    )
}
