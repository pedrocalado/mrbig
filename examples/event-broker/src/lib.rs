use async_channel::unbounded;
use common::auth;
use common::error;
use std::result::Result;

mod listeners;
use listeners::{ListenerRouter, RegisterRequest};

mod protos;
use protos::event_broker::event_broker_server::{EventBroker, EventBrokerServer};
use protos::event_broker::{
    produce_result, ConsumeRequest, EventStream, HistoryRequest, ProduceReply, ProduceRequest,
    ProduceResult, ProduceSuccess,
};

mod brokers;
use brokers::{HistoryConsumer, HistoryConsumerRequest, HistoryConsumerResponse, Producer};

mod multicast;

mod volatile;
use volatile::{volatile_start, VolatileConfig};

fn now() -> u64 {
    std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64
}

pub struct EventBrokerService {
    auth: auth::Auth,
    registry: ListenerRouter,
    producer: Box<dyn Producer + Send + Sync + 'static>,
    history: Box<dyn HistoryConsumer + Send + Sync + 'static>,
}

#[mrbig_derive::service_impl(tracing = "true", telemetry = "true")]
impl EventBroker for EventBrokerService {
    type HistoryStream = EventStream;
    type ConsumeStream = EventStream;

    async fn produce(
        &self,
        request: tonic::Request<ProduceRequest>,
    ) -> Result<tonic::Response<ProduceReply>, tonic::Status> {
        // Enforce authorization
        // if any event is unauthorized, exit immediately
        request
            .get_ref()
            .events
            .iter()
            .map(|ev| self.auth_check(request.metadata(), &ev.topic, Verb::Write))
            .collect::<Result<Vec<_>, tonic::Status>>()?;

        let timestamp = now();

        let request = request.into_inner();
        let results: Vec<Result<_, _>> =
            futures::future::join_all(request.events.into_iter().map(|event| {
                let mut event_mut = event;
                event_mut.timestamp = timestamp;
                self.producer.produce(event_mut)
            }))
            .await;

        // return one result per message produced
        Ok(tonic::Response::new(ProduceReply {
            results: results
                .into_iter()
                .map(|res| match res {
                    Ok(_) => ProduceResult {
                        inner: Some(produce_result::Inner::Ok(ProduceSuccess { timestamp })),
                    },
                    Err(err) => ProduceResult {
                        inner: Some(produce_result::Inner::Error(err.to_string())),
                    },
                })
                .collect(),
        }))
    }

    async fn history(
        &self,
        request: tonic::Request<HistoryRequest>,
    ) -> Result<tonic::Response<Self::HistoryStream>, tonic::Status> {
        // Enforce authorization
        self.auth_check(request.metadata(), &request.get_ref().topic, Verb::Read)?;

        let request = request.into_inner();
        let (tx, rx) = unbounded();

        let consume_history = HistoryConsumerRequest {
            request: request,
            tx,
        };

        self.history
            .consume(consume_history)
            .await
            .map_err(error::internal)?;

        let output = async_stream::stream! {
            while let recv = rx.recv().await {
                match recv {
                    Ok(inner) => match inner {
                        HistoryConsumerResponse::Event(event) => yield Ok(event),
                        HistoryConsumerResponse::Error(e) => {
                            yield Err(error::invalid_arg(e));
                            break;
                        },
                        HistoryConsumerResponse::Eof => { break; }
                    },
                    Err(e) => {
                        yield Err(error::internal(e));
                        break;
                    }
                }
            }
        };

        Ok(tonic::Response::new(Box::pin(output) as Self::HistoryStream))
    }

    async fn consume(
        &self,
        request: tonic::Request<ConsumeRequest>,
    ) -> Result<tonic::Response<Self::ConsumeStream>, tonic::Status> {
        // Enforce authorization
        self.auth_check(request.metadata(), &request.get_ref().pattern, Verb::Read)?;

        let request = request.into_inner();
        let (tx, rx) = unbounded();

        let register_request = RegisterRequest {
            consume_request: request,
            tx,
        };

        self.registry
            .register(register_request)
            .await
            .map_err(error::internal)?;

        let output = async_stream::stream! {
            while let recv = rx.recv().await {
                match recv {
                    Ok(event) => yield Ok(event),
                    Err(err) => {
                        yield Err(error::internal(err));
                        break;
                    }
                }
            }
        };

        Ok(tonic::Response::new(Box::pin(output) as Self::ConsumeStream))
    }
}

enum Verb {
    Read,
    Write,
}

impl EventBrokerService {
    fn auth_check(
        &self,
        metadata: &tonic::metadata::MetadataMap,
        _topic: &str,
        _verb: Verb,
    ) -> Result<(), tonic::Status> {
        if self.auth.is_disabled() {
            return Ok(());
        }

        let user = self.auth.get_user(metadata);
        if user.is_none() {
            return Err(error::unauth("must authenticate"));
        }

        return Ok(());
    }
}

use mrbig_derive::{Configurable, Run};

#[derive(Debug, Default, serde_derive::Deserialize)]
pub struct Flags {
    #[serde(default)]
    auth: auth::Auth,
    #[serde(default)]
    volatile: VolatileConfig,
}

// Use macro to register endpoints
#[derive(Run, Configurable)]
#[mrbig_register_grpc(service = "event_broker.EventBroker")]
pub struct Micro {
    context: mrbig_core::Context,
    #[mrbig_config_extra]
    flags: Box<Option<Flags>>,
}

#[cfg(not(test))]
pub async fn exec(args: Vec<String>) {
    // New service with default configurations
    let mut service = Micro {
        context: mrbig_core::Context::default(),
        flags: Box::new(Some(Flags::default())),
    };

    service
        .init_with_args(args)
        .await
        .expect("failed to initialize service");

    // Take the flags
    let flags = (*service.flags).take().unwrap();

    let auth = flags.auth;
    let volatile_config = flags.volatile;

    let registry = ListenerRouter::new();
    let (volatile_producer, volatile_history) = volatile_start(registry.clone(), volatile_config);
    let producer = Box::new(volatile_producer);
    let history = Box::new(volatile_history);

    service
        .run(EventBrokerService {
            auth,
            registry,
            producer,
            history,
        })
        .await
        .expect("failed to run service");
}

#[cfg(test)]
mod tests {
    use super::auth::Auth;
    use super::*;
    use futures::StreamExt;
    use protos::event_broker::{Event, SearchCriteria};
    use tokio::runtime::Runtime;

    const USER_FOO: &str = "foobar|012345678";
    const TEST_TOPIC: &str = "foobar";

    fn set_user_foo<T>(req: &mut tonic::Request<T>, auth: &Auth) {
        let mut mref = req.metadata_mut();
        auth.set_user(&mut mref, USER_FOO);
    }

    fn default_service() -> EventBrokerService {
        let registry = ListenerRouter::new();
        let (producer, history) = volatile_start(registry.clone(), VolatileConfig::default());
        // Create the event broker service
        EventBrokerService {
            auth: Auth::default(),
            registry,
            producer: Box::new(producer),
            history: Box::new(history),
        }
    }

    fn simple_event() -> Event {
        Event {
            topic: TEST_TOPIC.into(),
            key: "key".into(),
            payload: "abcd".into(),
            ..Default::default()
        }
    }

    fn event_with_payload(payload: &str) -> Event {
        Event {
            topic: TEST_TOPIC.into(),
            key: "key".into(),
            payload: payload.into(),
            ..Default::default()
        }
    }

    fn events_are_equal(a: &Event, b: &Event) -> bool {
        (a.topic == b.topic) && (a.key == b.key) && (a.payload == b.payload)
    }

    #[test]
    fn event_broker_unauth() {
        Runtime::new().unwrap().block_on(async move {
            let event_broker = default_service();

            // Unauthorized operation must fail
            assert!(event_broker
                .produce(tonic::Request::new(ProduceRequest {
                    events: vec![simple_event()]
                }))
                .await
                .is_err());

            // Unauthorized operation must fail
            assert!(event_broker
                .consume(tonic::Request::new(ConsumeRequest {
                    pattern: TEST_TOPIC.into(),
                }))
                .await
                .is_err());

            // Unauthorized operation must fail
            assert!(event_broker
                .history(tonic::Request::new(HistoryRequest {
                    topic: TEST_TOPIC.into(),
                    ..Default::default()
                }))
                .await
                .is_err());

            // authorized request must succeed
            let mut request = tonic::Request::new(ProduceRequest {
                events: vec![simple_event()],
            });
            set_user_foo(&mut request, &Auth::default());
            assert!(event_broker.produce(request).await.is_ok());
        });
    }

    async fn produce_events(service: &EventBrokerService, events: Vec<Event>) {
        let mut request = tonic::Request::new(ProduceRequest { events });
        set_user_foo(&mut request, &Auth::default());
        let response = service.produce(request).await;
        let response = response
            .expect("produce message should have succeeded")
            .into_inner();
        assert!(response.results.len() > 0);
        assert!(match response.results[0].inner {
            Some(produce_result::Inner::Ok(_)) => true,
            _ => false,
        });
    }

    #[test]
    fn event_broker_produce_consume() {
        Runtime::new().unwrap().block_on(async move {
            let auth = Auth::default();
            let event_broker = default_service();

            let consumer = async {
                let mut request = tonic::Request::new(ConsumeRequest {
                    pattern: TEST_TOPIC.into(),
                });
                set_user_foo(&mut request, &auth);
                // Start consuming
                event_broker.consume(request).await
            }
            .await;
            assert!(consumer.is_ok());

            let consumer = consumer.unwrap().into_inner();

            // Produce a few events
            let to_produce = vec![
                simple_event(),
                event_with_payload("baba"),
                event_with_payload("foobar"),
                simple_event(),
                event_with_payload("baba"),
                event_with_payload("foobar"),
            ];

            // Produce in two different requests
            produce_events(
                &event_broker,
                to_produce
                    .clone()
                    .into_iter()
                    .take(to_produce.len() / 2)
                    .collect(),
            )
            .await;
            produce_events(
                &event_broker,
                to_produce
                    .clone()
                    .into_iter()
                    .take(to_produce.len() / 2)
                    .collect(),
            )
            .await;

            // Make sure they've been consumed
            let items_consumed = consumer.take(to_produce.len()).collect::<Vec<_>>().await;

            let events = items_consumed
                .into_iter()
                .collect::<Result<Vec<Event>, _>>()
                .expect("failed to consume");

            to_produce
                .iter()
                .enumerate()
                .for_each(|(i, event)| assert!(events_are_equal(&events[i], event)));
        });
    }

    #[test]
    fn event_broker_history() {
        Runtime::new().unwrap().block_on(async move {
            let auth = Auth::default();
            let event_broker = default_service();

            // Produce a few events
            let to_produce = vec![
                event_with_payload("1"),
                event_with_payload("2"),
                event_with_payload("3"),
                event_with_payload("4"),
                event_with_payload("5"),
            ];

            produce_events(&event_broker, to_produce.clone()).await;

            tokio::time::delay_for(std::time::Duration::from_millis(500)).await;

            // Request plain history
            {
                let history = async {
                    let mut request = tonic::Request::new(HistoryRequest {
                        topic: TEST_TOPIC.into(),
                        criteria: Default::default(),
                    });
                    set_user_foo(&mut request, &auth);
                    // Start consuming
                    event_broker.history(request).await
                }
                .await
                .expect("history request must succeed")
                .into_inner();

                // Make sure they've been consumed
                let items_history = history.collect::<Vec<_>>().await;

                let events = items_history
                    .into_iter()
                    .collect::<Result<Vec<Event>, _>>()
                    .expect("failed to consume");

                to_produce
                    .iter()
                    .enumerate()
                    .for_each(|(i, event)| assert!(events_are_equal(&events[i], event)));
            }

            // Request reversed history
            {
                let history = async {
                    let mut criteria: SearchCriteria = Default::default();
                    criteria.newest_first = true;
                    let mut request = tonic::Request::new(HistoryRequest {
                        topic: TEST_TOPIC.into(),
                        criteria: Some(criteria),
                    });
                    set_user_foo(&mut request, &auth);
                    // Start consuming
                    event_broker.history(request).await
                }
                .await
                .expect("history request must succeed")
                .into_inner();

                // Make sure they've been consumed
                let items_history = history.collect::<Vec<_>>().await;

                let events = items_history
                    .into_iter()
                    .collect::<Result<Vec<Event>, _>>()
                    .expect("failed to consume");

                to_produce
                    .iter()
                    .rev()
                    .enumerate()
                    .for_each(|(i, event)| assert!(events_are_equal(&events[i], event)));
            }

            // Limited reversed history
            {
                let limit = 3 as usize;

                let history = async {
                    let mut criteria: SearchCriteria = Default::default();
                    criteria.newest_first = true;
                    criteria.limit = limit as u32;
                    let mut request = tonic::Request::new(HistoryRequest {
                        topic: TEST_TOPIC.into(),
                        criteria: Some(criteria),
                    });
                    set_user_foo(&mut request, &auth);
                    // Start consuming
                    event_broker.history(request).await
                }
                .await
                .expect("history request must succeed")
                .into_inner();

                // Make sure they've been consumed
                let items_history = history.collect::<Vec<_>>().await;

                let events = items_history
                    .into_iter()
                    .collect::<Result<Vec<Event>, _>>()
                    .expect("failed to consume");

                assert_eq!(events.len(), limit);

                to_produce
                    .iter()
                    .rev()
                    .take(limit)
                    .enumerate()
                    .for_each(|(i, event)| assert!(events_are_equal(&events[i], event)));
            }
        });
    }
}
