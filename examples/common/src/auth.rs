use tonic::metadata::{MetadataKey, MetadataMap, MetadataValue};

pub const DEFAULT_JWT_HEADER: &str = "jwt-payload";
pub const DEFAULT_JWT_USER_CLAIM: &str = "sub";

#[derive(Clone, Debug, serde_derive::Deserialize)]
pub struct Auth {
    #[serde(default)]
    disabled: bool,
    #[serde(default = "default_jwt_header")]
    jwt_header: String,
    #[serde(default = "default_jwt_user_claim")]
    jwt_user_claim: String,
}

fn default_jwt_header() -> String {
    DEFAULT_JWT_HEADER.into()
}

fn default_jwt_user_claim() -> String {
    DEFAULT_JWT_USER_CLAIM.into()
}

impl Default for Auth {
    fn default() -> Auth {
        serde_json::from_str("{}").unwrap()
    }
}

impl Auth {
    pub fn get_user(&self, map: &MetadataMap) -> Option<String> {
        use serde_json::value::Value;

        map.get(&self.jwt_header).and_then(|v| {
            base64::decode(v.as_bytes())
                .ok()
                .and_then(|pl| serde_json::from_slice::<Value>(&pl).ok())
                .and_then(|v| match v {
                    Value::Object(m) => m.get(&self.jwt_user_claim).and_then(|o| match o {
                        Value::String(s) => Some(s.to_owned()),
                        _ => None,
                    }),
                    _ => None,
                })
        })
    }

    pub fn copy_headers(&self, dest: &mut MetadataMap, src: &MetadataMap) {
        match src.get(&self.jwt_header) {
            Some(v) => {
                dest.insert(
                    MetadataKey::from_bytes(self.jwt_header.as_bytes()).unwrap(),
                    v.clone(),
                );
            }
            None => {}
        }
    }

    pub fn set_user(&self, dest: &mut MetadataMap, user: &str) {
        let claim = base64::encode(format!(r#"{{"{}": "{}" }}"#, self.jwt_user_claim, user));

        dest.insert(
            MetadataKey::from_bytes(self.jwt_header.as_bytes()).unwrap(),
            MetadataValue::from_str(&claim).unwrap(),
        );
    }

    pub fn is_disabled(&self) -> bool {
        self.disabled
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const STUPID_PAYLOAD: &str = "stupidpayload";

    fn empty_metadata() -> MetadataMap {
        MetadataMap::new()
    }

    fn bad_metadata() -> MetadataMap {
        let mut metadata = empty_metadata();
        metadata.insert(DEFAULT_JWT_HEADER, STUPID_PAYLOAD.parse().unwrap());
        metadata
    }

    fn base64_metadata() -> MetadataMap {
        let mut metadata = empty_metadata();
        metadata.insert(
            DEFAULT_JWT_HEADER,
            base64::encode(STUPID_PAYLOAD).parse().unwrap(),
        );
        metadata
    }

    fn json_metadata() -> MetadataMap {
        let mut metadata = empty_metadata();
        metadata.insert(
            DEFAULT_JWT_HEADER,
            base64::encode(r#"{"bar":"foo"}"#).parse().unwrap(),
        );
        metadata
    }

    fn json_obj_metadata() -> MetadataMap {
        let mut metadata = empty_metadata();
        let buf = format!(r#"{{"{}":[1,2,3]}}"#, DEFAULT_JWT_USER_CLAIM);
        metadata.insert(DEFAULT_JWT_HEADER, base64::encode(&buf).parse().unwrap());
        metadata
    }

    fn with_user_metadata() -> MetadataMap {
        let mut m = empty_metadata();
        let buf = format!(r#"{{"{}":"foo"}}"#, DEFAULT_JWT_USER_CLAIM);
        m.insert(DEFAULT_JWT_HEADER, base64::encode(&buf).parse().unwrap());
        m
    }

    #[test]
    fn auth_get_user() {
        let auth = Auth::default();

        assert_eq!(auth.get_user(&empty_metadata()), None);
        assert_eq!(auth.get_user(&bad_metadata()), None);
        assert_eq!(auth.get_user(&base64_metadata()), None);
        assert_eq!(auth.get_user(&json_metadata()), None);
        assert_eq!(auth.get_user(&json_obj_metadata()), None);
        assert_eq!(auth.get_user(&with_user_metadata()), Some("foo".into()));
    }

    #[test]
    fn auth_copy_headers() {
        let auth = Auth::default();

        // Empty in empty out
        {
            let src = empty_metadata();
            let mut dst = empty_metadata();
            auth.copy_headers(&mut dst, &src);

            assert_eq!(dst.len(), 0);
        }

        // Main purpose
        {
            let src = bad_metadata();
            let mut dst = empty_metadata();
            auth.copy_headers(&mut dst, &src);

            assert_eq!(
                dst.get(DEFAULT_JWT_HEADER).unwrap().to_str().unwrap(),
                STUPID_PAYLOAD
            );
        }

        // Invariance
        {
            const RAND_KEY: &str = "therand";
            const RAND_VALUE: &str = "therandvalue";

            let src = bad_metadata();

            let mut dst = empty_metadata();
            dst.insert(RAND_KEY, RAND_VALUE.parse().unwrap());

            auth.copy_headers(&mut dst, &src);

            assert_eq!(
                dst.get(DEFAULT_JWT_HEADER).unwrap().to_str().unwrap(),
                STUPID_PAYLOAD
            );

            assert_eq!(dst.get(RAND_KEY).unwrap().to_str().unwrap(), RAND_VALUE);
        }
    }
}
