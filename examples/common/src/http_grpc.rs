use crate::error;
use http;
use hyper::{body, Body, Response};
use tonic::{
    metadata::{KeyAndValueRef, MetadataMap},
    Status,
};

pub async fn into_grpc_status(response: Response<Body>) -> Status {
    let http_code = response.status().as_u16();

    let bytes = match body::to_bytes(response.into_body())
        .await
        .map(|b| b.to_vec())
        .map_err(error::internal)
    {
        Ok(bytes) => bytes,
        Err(e) => {
            return e;
        }
    };

    let msg = String::from_utf8_lossy(&bytes);

    match http_code {
        401 | 403 => error::unauth(msg),
        400 => error::invalid_arg(msg),
        404 => error::not_found(msg),
        c if c / 100 == 5 => error::internal(msg),
        _ => error::unknown(msg),
    }
}

pub fn from_grpc_headers(
    metadata: &MetadataMap,
    filter: Option<Vec<&str>>,
) -> http::HeaderMap<http::HeaderValue> {
    let mut headers: http::HeaderMap<http::HeaderValue> = http::HeaderMap::new();

    inject_grpc_headers(metadata, filter, &mut headers);
    headers
}

pub fn inject_grpc_headers(
    metadata: &MetadataMap,
    filter: Option<Vec<&str>>,
    headers_ref: &mut http::HeaderMap<http::HeaderValue>,
) {
    use std::str::FromStr;

    if filter.is_some() {
        filter
            .unwrap()
            .iter()
            .filter(|&key| metadata.contains_key(*key))
            .for_each(|&key| {
                let hkey = http::header::HeaderName::from_str(key);
                let value = metadata.get(key).unwrap().as_bytes();
                let hvalue = http::HeaderValue::from_bytes(value);
                if hkey.is_err() || hvalue.is_err() {
                    return;
                }
                headers_ref.insert(hkey.unwrap(), hvalue.unwrap());
            });
        return;
    }

    metadata.iter().for_each(|kv| match kv {
        KeyAndValueRef::Ascii(ref key, ref value) => {
            let hkey = http::header::HeaderName::from_str(key.as_str());
            let hvalue = http::HeaderValue::from_bytes((*value).as_bytes());
            if hkey.is_err() || hvalue.is_err() {
                return;
            }
            headers_ref.insert(hkey.unwrap(), hvalue.unwrap());
        }
        KeyAndValueRef::Binary(ref key, ref value) => {
            let hkey = http::header::HeaderName::from_bytes(key.as_ref());
            let hvalue = http::HeaderValue::from_bytes((*value).as_encoded_bytes());
            if hkey.is_err() || hvalue.is_err() {
                return;
            }
            headers_ref.insert(hkey.unwrap(), hvalue.unwrap());
        }
    });
}
