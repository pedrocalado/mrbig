use std::fmt::Display;
use tonic::{Code, Status};

pub type Result<T> = std::result::Result<T, Status>;

pub fn internal<T: Display>(msg: T) -> Status {
    Status::new(Code::Internal, msg.to_string())
}

pub fn invalid_arg<T: Display>(msg: T) -> Status {
    Status::new(Code::InvalidArgument, msg.to_string())
}

pub fn not_found<T: Display>(msg: T) -> Status {
    Status::new(Code::NotFound, msg.to_string())
}

pub fn denied<T: Display>(msg: T) -> Status {
    Status::new(Code::PermissionDenied, msg.to_string())
}

pub fn unauth<T: Display>(msg: T) -> Status {
    Status::new(Code::Unauthenticated, msg.to_string())
}

pub fn unimplemented<T: Display>(msg: T) -> Status {
    Status::new(Code::Unimplemented, msg.to_string())
}

pub fn unknown<T: Display>(msg: T) -> Status {
    Status::new(Code::Unknown, msg.to_string())
}
