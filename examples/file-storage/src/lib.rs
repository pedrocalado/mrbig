use common::auth;
use common::error;
use std::result::Result;

mod protos;
use protos::file_storage::objects_server::{Objects, ObjectsServer};
use protos::file_storage::{GetObjectReply, GetObjectRequest, PutObjectReply, PutObjectRequest};

mod storage;
#[cfg(test)]
use storage::mocked::Mocked as Storage;
#[cfg(not(test))]
use storage::Storage;

mod operations;
use operations::Operations;

const IMAGES_PREFIX: &str = "images";
const GENERIC_PREFIX: &str = "generic";

#[derive(Debug)]
pub struct FileStorage {
    auth: auth::Auth,
    storage: Storage,
    storage_bucket_url: String,
    link_prefix: String,
    operations: Operations,
}

impl FileStorage {
    fn internal_path(&self, path: &str, user: &str, content_type: &str) -> String {
        let extension = std::path::Path::new(&path)
            .extension()
            .and_then(|os| os.to_str())
            .map(|s| format!(".{}", s))
            .unwrap_or("".to_string());

        // Use uuid for a random file name
        let id = uuid::Uuid::new_v4().to_hyphenated();

        let prefix = match content_type.starts_with("image/") {
            true => IMAGES_PREFIX,
            false => GENERIC_PREFIX,
        };

        format!("{}/{}/{}{}", prefix, user, id, extension)
    }

    fn validate_path(&self, path: &str) -> Result<(), tonic::Status> {
        if path.is_empty() {
            return Err(error::invalid_arg("empty path not allowed"));
        }

        if path.ends_with("/") {
            return Err(error::invalid_arg("path cannot end with a slash"));
        }

        Ok(())
    }
}

#[mrbig_derive::service_impl(tracing = "true", telemetry = "true")]
impl Objects for FileStorage {
    async fn get_object(
        &self,
        request: tonic::Request<GetObjectRequest>,
    ) -> Result<tonic::Response<GetObjectReply>, tonic::Status> {
        // Unauthorized user can perform Gets
        let authenticated = self.auth.get_user(request.metadata()).is_some();

        let request = request.into_inner();

        self.validate_path(&request.path)?;

        // If operations are set, get image through imaginary.
        // Otherwise go directly to storage.
        let object = match request.operation {
            Some(operation) => {
                // Unauthorized user cannot perform operations
                if !authenticated {
                    return Err(error::unauth("must authenticate"));
                }

                let from_storage = self.storage.get(&request.path).await?;

                self.operations
                    .raw_source(from_storage.body, &from_storage.content_type, operation)
                    .await?
            }
            None => self.storage.get(&request.path).await?,
        };

        Ok(tonic::Response::new(GetObjectReply {
            buffer: object.body,
            content_type: object.content_type,
        }))
    }

    async fn put_object(
        &self,
        request: tonic::Request<PutObjectRequest>,
    ) -> Result<tonic::Response<PutObjectReply>, tonic::Status> {
        let user = self.auth.get_user(request.metadata());
        // Unauthorized user cannot put objects
        if user.is_none() {
            return Err(error::unauth("must authenticate"));
        }

        let request = request.into_inner();

        let storage::Object { body, content_type } = match request.operation {
            Some(operation) => {
                log::info!(
                    "performing operation: {:?} with {}",
                    operation,
                    request.content_type
                );
                self.operations
                    .raw_source(request.buffer, &request.content_type, operation)
                    .await?
            }
            None => storage::Object {
                content_type: request.content_type,
                body: request.buffer,
            },
        };

        log::info!("operation yielded: {:?}", content_type);

        let path = self.internal_path(&request.name, &user.unwrap(), &content_type);

        self.storage.put(&path, &body, &content_type).await?;

        Ok(tonic::Response::new(PutObjectReply {
            path: path.clone(),
            link: format!("{}/{}", self.link_prefix, path),
        }))
    }
}

use mrbig_derive::{Configurable, Run};

#[derive(Debug, Default, serde_derive::Deserialize)]
pub struct Flags {
    #[serde(default)]
    auth: auth::Auth,
    #[serde(default, rename = "storage")]
    storage_config: storage::Config,
    #[serde(default, rename = "operations")]
    operations_config: operations::Config,
    #[serde(default, rename = "link_prefix")]
    link_prefix: String,
}

// Use macro to register endpoints
#[derive(Run, Configurable)]
#[mrbig_register_grpc(service = "file_storage.Objects")]
pub struct Micro {
    context: mrbig_core::Context,
    #[mrbig_config_extra]
    flags: Box<Option<Flags>>,
}

#[cfg(not(test))]
pub async fn exec(args: Vec<String>) {
    // New service with default configurations
    let mut service = Micro {
        context: mrbig_core::Context::default(),
        flags: Box::new(Some(Flags::default())),
    };

    service
        .init_with_args(args)
        .await
        .expect("failed to initialize service");

    // Take the flags
    let flags = (*service.flags).take().unwrap();

    let storage_bucket_url = format!(
        "{}/{}",
        flags.storage_config.region_endpoint, flags.storage_config.bucket
    );
    let storage = Storage::new(flags.storage_config).await.unwrap();

    let operations = Operations::new(flags.operations_config);
    let link_prefix = flags.link_prefix;

    let auth = flags.auth;

    service
        .run(FileStorage {
            auth,
            storage,
            storage_bucket_url,
            operations,
            link_prefix,
        })
        .await
        .expect("failed to run service");
}

#[cfg(test)]
mod tests {
    use super::auth::Auth;
    use super::operations::{Config, Operations};
    use super::protos::file_storage::{objects_server::Objects, operation::Kind, Operation};
    use super::storage::mocked::Mocked as Storage;
    use super::FileStorage;
    use super::{GetObjectRequest, PutObjectRequest};
    use super::{GENERIC_PREFIX, IMAGES_PREFIX};
    use tokio::runtime::Runtime;

    const USER_FOO: &str = "foobar|012345678";
    const LINK_PREFIX: &str = "https://foobar.com/";

    fn set_user_foo<T>(req: &mut tonic::Request<T>, auth: &Auth) {
        let mut mref = req.metadata_mut();
        auth.set_user(&mut mref, USER_FOO);
    }

    #[test]
    fn file_storage_simple() {
        // Create the file storage service
        let file_storage = FileStorage {
            auth: Auth::default(),
            storage: Storage::new(),
            storage_bucket_url: "".into(),
            operations: Operations::new(Config {
                imaginary_endpoint: "".into(),
            }),
            link_prefix: LINK_PREFIX.into(),
        };

        Runtime::new().unwrap().block_on(async move {
            // Nonexisting path must fail
            assert!(file_storage
                .get_object(tonic::Request::new(GetObjectRequest {
                    path: "none".into(),
                    operation: None,
                }))
                .await
                .is_err());

            // Unauthorized put must fail
            assert!(file_storage
                .put_object(tonic::Request::new(PutObjectRequest {
                    name: "foobar".into(),
                    buffer: "contents".into(),
                    content_type: "text/plain".into(),
                    operation: None,
                }))
                .await
                .is_err());

            // Unauthorized get with operations must fail
            assert!(file_storage
                .get_object(tonic::Request::new(GetObjectRequest {
                    path: "none".into(),
                    operation: Some(Operation {
                        params: r#"{}"#.into(),
                        kind: Some(Kind::Imaginary("crop".into())),
                    }),
                }))
                .await
                .is_err());

            // Invalid path must fail
            assert!(file_storage
                .get_object(tonic::Request::new(GetObjectRequest {
                    path: "none/".into(),
                    operation: None,
                }))
                .await
                .is_err());

            // Invalid path must fail
            assert!(file_storage
                .get_object(tonic::Request::new(GetObjectRequest {
                    path: "".into(),
                    operation: None,
                }))
                .await
                .is_err());
        });
    }

    #[test]
    fn file_storage_put_get_generic() {
        let auth = Auth::default();
        let fake_contents: Vec<u8> = "contents".into();
        let content_type: String = "text/plain".into();
        let extension: &str = ".txt";
        let name: String = format!("foobar.{}", extension);

        // Authorized put
        let mut req_put_auth = tonic::Request::new(PutObjectRequest {
            name: name.clone(),
            buffer: fake_contents.clone(),
            content_type: content_type.clone(),
            operation: None,
        });
        set_user_foo(&mut req_put_auth, &auth);

        // Create the file storage service
        let file_storage = FileStorage {
            auth,
            storage: Storage::new(),
            storage_bucket_url: "".into(),
            operations: Operations::new(Config {
                imaginary_endpoint: "".into(),
            }),
            link_prefix: LINK_PREFIX.into(),
        };

        // Put and get a generic file
        Runtime::new().unwrap().block_on(async move {
            // Must succeed
            let put_response = file_storage
                .put_object(req_put_auth)
                .await
                .unwrap()
                .into_inner();

            let expected_prefix = format!("{}/{}/", GENERIC_PREFIX, USER_FOO);
            assert!(put_response.path.starts_with(&expected_prefix));
            assert!(put_response.path.ends_with(&extension));
            assert!(put_response.link.starts_with(LINK_PREFIX));

            // Unauthenticated get of the same object (must succeed)
            let req_get_auth = tonic::Request::new(GetObjectRequest {
                path: put_response.path,
                operation: None,
            });

            let get_response = file_storage
                .get_object(req_get_auth)
                .await
                .unwrap()
                .into_inner();

            assert_eq!(get_response.content_type, content_type);
            assert_eq!(get_response.buffer, fake_contents);
        });
    }

    #[test]
    fn file_storage_put_get_image() {
        let auth = Auth::default();
        let fake_contents: Vec<u8> = "contents".into();
        let content_type: String = "image/jpeg".into();
        let extension: &str = ".jpeg";
        let name: String = format!("foobar.{}", extension);

        // Authorized put
        let mut req_put_auth = tonic::Request::new(PutObjectRequest {
            name: name.clone(),
            buffer: fake_contents.clone(),
            content_type: content_type.clone(),
            operation: None,
        });
        set_user_foo(&mut req_put_auth, &auth);

        // Create the file storage service
        let file_storage = FileStorage {
            auth,
            storage: Storage::new(),
            storage_bucket_url: "".into(),
            operations: Operations::new(Config {
                imaginary_endpoint: "".into(),
            }),
            link_prefix: LINK_PREFIX.into(),
        };

        Runtime::new().unwrap().block_on(async move {
            // Must succeed
            let put_response = file_storage
                .put_object(req_put_auth)
                .await
                .unwrap()
                .into_inner();

            let expected_prefix = format!("{}/{}/", IMAGES_PREFIX, USER_FOO);
            assert!(put_response.path.starts_with(&expected_prefix));
            assert!(put_response.path.ends_with(&extension));
            assert!(put_response.link.starts_with(LINK_PREFIX));

            // Unauthenticated get of the same object (must succeed)
            let req_get_auth = tonic::Request::new(GetObjectRequest {
                path: put_response.path,
                operation: None,
            });

            let get_response = file_storage
                .get_object(req_get_auth)
                .await
                .unwrap()
                .into_inner();

            assert_eq!(get_response.content_type, content_type);
            assert_eq!(get_response.buffer, fake_contents);
        });
    }
}
