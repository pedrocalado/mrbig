use crate::protos::file_storage;
use crate::storage::Object;
use common::error;
use hyper::{
    client::{Client, HttpConnector},
    Body, Request, Response,
};
use serde_json::Map;

#[derive(Debug)]
pub struct Operations {
    client: Client<HttpConnector, Body>,
    uri: String,
}

#[derive(Clone, Debug, serde_derive::Deserialize)]
pub struct Config {
    pub imaginary_endpoint: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            imaginary_endpoint: "http://localhost:9001".into(),
        }
    }
}

async fn response_to_error(response: Response<Body>) -> tonic::Status {
    let http_code = response.status().as_u16();

    let bytes = match hyper::body::to_bytes(response.into_body())
        .await
        .map(|b| b.to_vec())
        .map_err(|e| error::internal(&e.to_string()))
    {
        Ok(bytes) => bytes,
        Err(e) => {
            return e;
        }
    };

    let msg = String::from_utf8_lossy(&bytes);

    match http_code {
        401 | 403 => error::unauth(&msg),
        400 => error::invalid_arg(&msg),
        404 => error::not_found(&msg),
        c if c / 100 == 5 => error::internal(&msg),
        _ => error::unknown(&msg),
    }
}

async fn response_to_object(response: Response<Body>) -> Result<Object, tonic::Status> {
    let content_type = response
        .headers()
        .get("content-type")
        .ok_or(error::internal("no content-type"))?
        .to_str()
        .map_err(|e| error::internal(&e.to_string()))?
        .to_string();

    let http_code = response.status().as_u16();

    if http_code / 100 != 2 {
        return Err(response_to_error(response).await);
    }

    let body = hyper::body::to_bytes(response.into_body())
        .await
        .map(|b| b.to_vec())
        .map_err(|e| error::internal(&e.to_string()))?;

    Ok(Object { body, content_type })
}

impl Operations {
    pub fn new(config: Config) -> Operations {
        Operations {
            client: Client::new(),
            uri: config.imaginary_endpoint,
        }
    }

    fn imaginary_url(&self, params: Map<String, serde_json::Value>, op_name: String) -> String {
        use url::form_urlencoded::Serializer;

        let query = {
            let mut encoded = Serializer::new(String::new());
            params.into_iter().for_each(|(k, v)| {
                let as_string = match v {
                    serde_json::Value::String(s) => s,
                    _ => v.to_string(),
                };
                encoded.append_pair(k.as_str(), &as_string);
            });
            encoded.finish()
        };

        format!("{}/{}?{}", self.uri.clone(), format!("{}", op_name), query)
    }

    fn imaginary_post_request(
        &self,
        url: String,
        raw: Vec<u8>,
        content_type: &str,
    ) -> Result<Request<Body>, tonic::Status> {
        Request::builder()
            .method("POST")
            .header("Content-Type", content_type)
            .uri(url)
            .body(Body::from(raw))
            .map_err(|e| error::internal(&e.to_string()))
    }

    pub async fn raw_source(
        &self,
        raw: Vec<u8>,
        content_type: &str,
        operation: file_storage::Operation,
    ) -> Result<crate::storage::Object, tonic::Status> {
        if operation.kind.is_none() {
            return Err(error::invalid_arg("unspecified operation kind"));
        }

        let op_name = match operation.kind.unwrap() {
            file_storage::operation::Kind::Imaginary(n) => n,
            _ => {
                return Err(error::unimplemented("operation kind not implemented"));
            }
        };

        let params: Map<String, serde_json::Value> = serde_json::from_str(&operation.params)
            .map_err(|e| error::invalid_arg(&e.to_string()))?;

        let imaginary_url = self.imaginary_url(params, op_name);
        log::info!("requesting to {}", imaginary_url);

        let request = self.imaginary_post_request(imaginary_url, raw, content_type)?;

        let response = self
            .client
            .request(request)
            .await
            .map_err(|e| error::internal(&e.to_string()))?;

        response_to_object(response).await
    }
}
