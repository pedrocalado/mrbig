#[cfg(not(test))]
#[tokio::main]
async fn main() {
    file_storage::exec(std::env::args().collect()).await;
}
