use awscreds::Credentials;
use awsregion::Region;
use common::error;
use s3::bucket::Bucket;

const CONTENT_TYPE: &str = "content-type";

#[derive(Debug)]
pub struct Storage {
    bucket: s3::bucket::Bucket,
}

#[derive(Clone, Debug, serde_derive::Deserialize)]
pub struct Config {
    pub bucket: String,
    pub region_name: String,
    pub region_endpoint: String,
    pub access_key: String,
    pub secret_key: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            bucket: "mybucket".into(),
            region_name: "us-east-1".into(),
            region_endpoint: "http://localhost:9000".into(),
            access_key: "minioadmin".into(),
            secret_key: "minioadmin".into(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Object {
    pub content_type: String,
    pub body: Vec<u8>,
}

impl Storage {
    pub async fn new(config: Config) -> Result<Storage, Box<dyn std::error::Error>> {
        let bucket = Bucket::new(
            &config.bucket,
            Region::Custom {
                region: config.region_name,
                endpoint: config.region_endpoint,
            },
            Credentials::new(Some(config.access_key), Some(config.secret_key), None, None)
                .await
                .unwrap(),
        )
        .unwrap();

        Ok(Storage { bucket })
    }

    pub async fn get(&self, path: &str) -> Result<Object, tonic::Status> {
        let breq = s3::request::Request::new(&self.bucket, &path, s3::command::Command::GetObject);

        let resp = breq
            .response_future()
            .await
            .map_err(|e| error::internal(&e.to_string()))?;

        let code = resp.status().as_u16();
        let content_type = resp
            .headers()
            .get(CONTENT_TYPE)
            .map(|value| value.to_str().unwrap_or(""))
            .map(|s| s.to_string())
            .unwrap_or("".to_string());

        let body = resp
            .bytes()
            .await
            .map_err(|e| error::internal(&e.to_string()))?
            .to_vec();

        match code {
            200 => Ok(Object { content_type, body }),
            404 => Err(error::not_found(&format!("{} not found", path))),
            401 | 403 => {
                let text_response = String::from_utf8_lossy(&body);
                Err(error::unauth(&text_response))
            }
            _ => {
                let text_response = String::from_utf8_lossy(&body);
                log::error!("storage get response ({}): {}", code, text_response);
                Err(error::internal(&text_response))
            }
        }
    }

    pub async fn put(
        &self,
        path: &str,
        buffer: &[u8],
        content_type: &str,
    ) -> Result<(), tonic::Status> {
        let (body, code) = self
            .bucket
            .put_object(path, buffer, content_type)
            .await
            .map_err(|e| error::internal(&e.to_string()))?;

        let text_response = String::from_utf8_lossy(&body);

        if code / (100 as u16) != 2 {
            log::error!("storage put response ({}): {}", code, text_response);
            return Err(error::internal(&text_response));
        }

        log::info!("storage put response: {:?}", text_response);
        Ok(())
    }
}

#[cfg(test)]
pub mod mocked {
    use super::Object;
    use common::error;
    use futures::lock::Mutex;
    use std::collections::HashMap;
    use std::sync::Arc;

    #[derive(Debug)]
    pub struct Mocked {
        pub store: Arc<Mutex<HashMap<String, Object>>>,
    }

    impl Mocked {
        pub fn new() -> Mocked {
            Mocked {
                store: Arc::new(Mutex::new(HashMap::new())),
            }
        }

        pub async fn get(&self, path: &str) -> Result<Object, tonic::Status> {
            let map = self.store.lock().await;
            match map.get(path) {
                None => Err(error::not_found("path not found")),
                Some(o) => Ok(o.clone()),
            }
        }

        pub async fn put(
            &self,
            path: &str,
            buffer: &[u8],
            content_type: &str,
        ) -> Result<(), tonic::Status> {
            let mut map = self.store.lock().await;
            map.insert(
                path.into(),
                Object {
                    content_type: content_type.into(),
                    body: buffer.into(),
                },
            );
            Ok(())
        }
    }
}
