# Useful Links

This appendix provides some useful web links to open source projects, tools or third-party libraries.

## Identity, Authentication and Access Control

- [Cierge password-less authentication server](https://github.com/PwdLess/Cierge)

## Google Suite Add-ons Development

- [Extending G Suite with Add-ons](https://developers.google.com/gsuite/add-ons/overview)

## Bazel

[LLVM Toolchain for Bazel](https://github.com/grailbio/bazel-toolchain)