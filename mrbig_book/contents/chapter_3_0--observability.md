Actionable (financial) content service aims at producing content that is sensitive to its context. As a subtopic of content intelligence, it provides with a set of tools for the financial practice in the digital age.

In this introductory chapter, the followint topics are covered:

- Getting started with development environment
- Architecture and components
- TODO