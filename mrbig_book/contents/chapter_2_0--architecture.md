Doddy's architecture relies on the [Command Query Responsibility Segregation (CQRS) architecture](https://martinfowler.com/bliki/CQRS.html) pattern, that advocates separating queries, namely read operations, from commands or write operations. The CQRS pattern works gracefully with event-based programming model and [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html) concept, the latter allowing to track the state's life-cycle of services.

In this chapter, the followint topics are covered:

- Discovering design rationale behind Doddy and why we decided to apply the CQRS pattern to specific bounded contexts or the project, such as, for instance, the AI pipeline.
- Describing architecture and components of the overall Doddy ecosystem
- Explaining application state's life-cycle serialization, using Event Sourcing pattern
