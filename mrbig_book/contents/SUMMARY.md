# The MrBig Book

[Foreword](foreword.md)

## Getting Started

- [Introduction](chapter_1_0--introduction.md)
  - [Getting Started](chapter_1_1--getting_started.md)
- [Architecture and Design Rationale](chapter_2_0--architecture.md)
  - [Commands and Queries Segregation Pattern](chapter_2_1--cqrs_architecture.md)
  - [Services Orchestration and Meshing](chapter_2_1--services_orchestration_meshing.md)
- [Observability and Control](chapter_3_0--observability.md)
- [Appendix](appendix.md)
  - [A - Glossary](appendix_a_0--glossary.md)
    - [Acronyms](appendix_a_1--acronyms.md)
    - [Terms](appendix_a_2--terms.md)
  - [B - References](appendix_b_0--references.md)
    - [Bibliography](appendix_b_1--bibliography.md)
    - [Useful links](appendix_b_2--useful_links.md)


